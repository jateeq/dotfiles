### README ###

These are my environment dotfiles. Note that the config files were not all written by me, these have been picked and pruned from multiple resources.

- Copies over specified files to a backup folder
- Copies the specified files into a folder called ~/dotfiles
- Creates symbolic links in ~/ to the specified files
- Checks for zsh installation (if not installed, tries to install it)

### Usage ###
Simply clone the directory and run the installenv.sh file.