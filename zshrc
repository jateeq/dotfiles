# Path to your oh-my-zsh installation.
export ZSH=/Users/jateeq/.oh-my-zsh

# Set name of the theme to lo
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

# User configuration

export PATH="/usr/local/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


# Use vim for editing config files
alias zshconf="vim ~/.zshrc"
alias sourcezsh="source ~/.zshrc"

# Note that the zsh-syntax highlighting must be the last plugin sourced
plugins=(git colored-man colorize github jira vagrant virtualenv pip python brew osx zsh-syntax-highlighting)

# Set user specific aliases 
alias vimconf="vim ~/.vimrc"
alias dev="cd ~/dev"
alias gitgraph="git log --graph --abbrev-commit --decorate --format=:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias pytest="py.test"

# Alias for icdiff as a git diff tool
alias giticdiff="git difftool --no-prompt --extcmd icdiff"

# Alias to convert from markdownt to html
# sample use: mdtohtml readme.md > readme.html
alias mdtohtml=pandoc -f markdown -t html

# prev bashrc content
export BASH_CONF="bashrc"
export WORK_ON=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/dev
source /usr/local/bin/virtualenvwrapper.sh

export USER_NAME="Jawad Ateeq"

# FileSearch
function f() { find . -iname "*$1*" ${@:2} }
function r() { grep "$1" ${@:2} -R . }

#mkdir and cd
function mkcd() { mkdir -p "$@" && cd "$_"; }

# allow zsh command line editing in vi mode
bindkey -v

workon ml
eval $(thefuck --alias)
deactivate

export BASH_CONF="bash_profile"

# Homebrew
export PATH=/usr/local/bin:$PATH

# Virtualenv / Virtualenvwrapper
source /usr/local/bin/virtualenvwrapper.sh

# Don't let pip install unless in a virtual environemtn
export PIP_REQUIRE_VIRTUALENV=True

# Add project folder to pythonpath
export PYTHONPATH=$PYTHONPATH:~/dev
export PYTHONPATH=$PYTHONPATH:~/dev/fact_checker
export PYTHONPATH=$PYTHONPATH:~/dev/twitter_sentiment_analysis


# Prettify terminal at startup :)
touch ~/.hushlogin
echo "Do things right the first time" | cowsay -f dragon

