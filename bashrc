export BASH_CONF="bashrc"
export WORK_ON=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/dev
source /usr/local/bin/virtualenvwrapper.sh

workon ml
eval $(thefuck --alias)
deactivate

export BASH_CONF="bash_profile"

# Homebrew
export PATH=/usr/local/bin:$PATH

# Virtualenv / Virtualenvwrapper
source /usr/local/bin/virtualenvwrapper.sh

# Don't let pip install unless in a virtual environemtn
export PIP_REQUIRE_VIRTUALENV=True

# Add project folder to pythonpath
export PYTHONPATH=$PYTHONPATH:~/dev
export PYTHONPATH=$PYTHONPATH:~/dev/fact_checker
export PYTHONPATH=$PYTHONPATH:~/dev/lab_twitter_sentiment_analysis
